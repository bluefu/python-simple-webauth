from typing import List
from flask import Flask, Blueprint

from exceptions import ResourceException, ResourceNotFound, ResourceNotImplemented, create_response


class FlaskExceptionConfig:
    def __init__(self, flask_app):
        self.app = flask_app

    def configure_handlers(self):
        self.register_exception(ResourceException)
        self.register_exception(ResourceNotImplemented)
        self.register_exception(ResourceNotFound)

    def register_exception(self, cls):
        self.app.register_error_handler(cls, lambda e: create_response(e))

    def configure_app(self):
        self.configure_handlers()


def apply_servername(response):
    response.headers['server'] = 'some_server'
    return response


def post_configuration(app: Flask, configs):
    app.after_request_funcs[None].append(apply_servername)
    for _config in configs:
        _config.configure_app()


def register_blueprints(app: Flask, blueprints: List[Blueprint]):
    for bp in blueprints:
        app.register_blueprint(bp)
