from flask import Flask, Blueprint
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy

def create_app():
    app = Flask(__name__)
    CORS(app, allow_headers="*")
    app.config.from_pyfile('default_config.py')
    app.config.from_pyfile('development.py')
    return app


def get_db(app: Flask) -> SQLAlchemy:
    return SQLAlchemy(app)


def get_db_session(app: Flask):
    _db = get_db(app)
    return _db.session


def get_secret_key(app: Flask):
    return app.config['SECRET_KEY']


FLASK_APP = create_app()
DB = get_db(FLASK_APP)