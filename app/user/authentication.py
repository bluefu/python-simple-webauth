import datetime
import jwt
from flask import Blueprint, request
from flask.views import MethodView
from flask_restful import Api
from werkzeug.security import check_password_hash

from database_model import User
from config import FLASK_APP, get_secret_key
from user.request_model import get_credentials_fields
from exceptions import AuthenticationFailed
from utils.basic import is_dict_structure_equal
from utils.http import get_login_response


API_PREFIX = 'login'
AUTHENTICATION_BP = Blueprint('login_api', __name__)
api = Api(AUTHENTICATION_BP)


class LoginAPI(MethodView):
    def post(self):
        data = request.get_json()

        if not is_dict_structure_equal(get_credentials_fields(), data):
            raise AuthenticationFailed('Could not verify')

        username = data['username']
        password = data['password']

        if not username or not password:
            raise AuthenticationFailed('Could not verify')
        user = User.query.filter_by(name=username).first()

        if not user:
            raise AuthenticationFailed('Could not verify')

        if check_password_hash(user.password, password):
            token = jwt.encode({'public_id': user.public_id, 'exp': datetime.datetime.utcnow(
                 ) + datetime.timedelta(minutes=30)}, get_secret_key(FLASK_APP), algorithm='HS256')
            return get_login_response(dict(
                token=token.decode('UTF-8')
            ))
        raise AuthenticationFailed('Could not verify')


api.add_resource(LoginAPI, '/{rsc}'.format(rsc=API_PREFIX))
