from flask_restful import fields

def get_registered_user_details():
    _user_fields = {
        'public_id': fields.String,
        'name': fields.String,
        'email': fields.String
    }
    return _user_fields


def get_token_fields():
    _token_fields = {
        'token': fields.String
    }
    return _token_fields
