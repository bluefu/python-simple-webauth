from flask_restful import fields

def get_credentials_fields():
    _credentials_fields = {
        'username': fields.String,
        'password': fields.String
    }
    return _credentials_fields


def get_user_request_fields():
    _user_request_fields = {
        'name': fields.String,
        'email': fields.String,
        'new_password': fields.String,
        'old_password': fields.String
    }
    return _user_request_fields
