from flask import Flask

from user.authentication import AUTHENTICATION_BP
from user.user_resource import USER_BP
from config import FLASK_APP
from config.flask_config import FlaskExceptionConfig, post_configuration, register_blueprints


def start_app(app: Flask):
    _EXCEPTION_CONF = FlaskExceptionConfig(app)
    _blueprints = [
        AUTHENTICATION_BP,
        USER_BP
    ]
    _configs = [
        _EXCEPTION_CONF
    ]
    post_configuration(app, _configs)
    register_blueprints(app, _blueprints)
    _host = app.config['HOST']
    _port = app.config['PORT']
    _threaded = True
    app.run(host=_host, port=_port, threaded=_threaded)


if __name__ == '__main__':
    start_app(FLASK_APP)