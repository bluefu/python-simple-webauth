from flask import jsonify

class ResourceException(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None, headers: dict=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload
        self.headers = headers

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv


class ResourceNotFound(ResourceException):
    def __init__(self, message, payload=None, headers=None):
        ResourceException.__init__(self, message, 404, payload, headers)


class AuthenticationFailed(ResourceException):
    def __init__(self, message, payload=None, headers=None):
        ResourceException.__init__(self, message, 401, payload, headers)

class ResourceNotImplemented(ResourceException):
    def __init__(self, message='Not implemented yet', payload=None, headers=None):
        ResourceException.__init__(self, message, 501, payload, headers)


def create_response(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    if error.headers:
        for k, v in error.headers.items():
            response.headers[k] = v
    return response
