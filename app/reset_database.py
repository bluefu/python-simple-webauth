from database_model import DB


def reset_database():
    DB.drop_all()
    DB.create_all()


reset_database()
