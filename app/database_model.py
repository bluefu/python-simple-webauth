from config import DB
import enum


class User(DB.Model):
    __tablename__ = 'user'

    # Attributes #
    id = DB.Column(DB.Integer, primary_key=True)
    public_id = DB.Column(DB.String(100), unique=True, nullable=False)
    password = DB.Column(DB.String(100), nullable=False)
    name = DB.Column(DB.String(50), nullable=False, unique=True)
    email = DB.Column(DB.String(50), nullable=False, unique=True)
