## Setup
How to setup this sample project.
### Python Environment
* This project uses **python 3.6 or higher**.
* Check your default `python` installation with: `python --version`
* If the default python installation is lower than 3.6, use custom python commands. (e.g. `python3.6`, `python3`, etc.)
* `python -m venv venv`: Installs the virtual environment in a new folder called `venv` inside the current directory.
* Activate the virtual environment:
    * Linux/MacOS: `source venv/bin/activate`
    * Windows: `source venv/Scripts/activate`
* `pip install -r requirements.txt`: Install all dependencies inside the virtual environment.
### Database
* `docker` and `docker-compose` have to be installed.
* `docker-compose up`: Starts the DB and the administation tool.
* `python reset-database.py`: Initializes Schemas of DB container.
* `docker exec -it <mysql_container_name> bash /sql/init.bash`: Inserts initial data in DB container.

## Usage
* **Important**: Activate `venv`, start the docker container and make sure to use the correct python installation.
* `python core.py`: Starts the application on port [http://localhost:5000](http://localhost:5000)
* How to login:
    * POST [http://localhost:5000/login](http://localhost:5000/login)
    * Request Body: `{ "username":"admin", "password":"password" }`
    * Returns: `{ "token":"${some_token}" }`
    * Token is valid for 30 minutes
* How to get the current user:
    * GET [http://localhost:5000/user/current](http://localhost:5000/user/current)
    * Headers: `x-access-token = ${some_token}`
    * Returns: `{ "public_id":"${some_uuid}", "name":"${some_name}", "email":"${some_email}" }`
    * For this request you have to set a valid token for the `x-access-token` header.
    * You receive a token from the login method.
* How to edit the current user:
    * PUT [http://localhost:5000/user/current](http://localhost:5000/user/current)
    * Request Body: `{ "name":"${some_name}", "email":"${some_email}", "new_password":"${some_new_password}", "old_password":"${some_old_password}" }`
    * Returns: `{ "public_id":"${some_uuid}", "name":"${some_name}", "email":"${some_email}" }`
    * For this request you have to set a valid token for the `x-access-token` header.
    * You receive a token from the login method.